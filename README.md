datalife_sale_invoice_product_as_service
========================================

The sale_invoice_product_as_service module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-sale_invoice_product_as_service/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-sale_invoice_product_as_service)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
