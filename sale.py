# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import fields
from trytond.pyson import Eval, Bool, Or
from trytond.transaction import Transaction
from trytond import backend

__all__ = ['SaleLine']


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'

    sale_as_service = fields.Boolean('Sale as service',
        states={
            'readonly': Or(~Bool(Eval('product_sale_as_service')),
                ~Eval('sale_state').in_(['draft'])),
            'invisible': ~Bool(Eval('product_sale_as_service'))
        }, depends=['product_sale_as_service', 'sale_state'])
    product_sale_as_service = fields.Function(
        fields.Boolean('Product sale as service'),
        'on_change_with_product_sale_as_service')
    sale_service = fields.Many2One('product.product', 'Sale service',
        domain=[('type', '=', 'service')],
        states={
            'readonly': Or(~Bool(Eval('product_sale_as_service')),
                (Eval('sale_state') != 'draft')),
            'invisible': Or(~Bool(Eval('product_sale_as_service')),
                ~Eval('sale_as_service')),
            'required': Bool(Eval('sale_as_service')),
        }, depends=['product_sale_as_service', 'sale_state', 'sale_as_service']
    )

    @classmethod
    def __register__(cls, module_name):
        pool = Pool()
        table_h = cls.__table_handler__(module_name)
        Product = pool.get('product.product')
        Template = pool.get('product.template')
        cursor = Transaction().connection.cursor()
        sql_table = cls.__table__()
        product = Product.__table__()
        template = Template.__table__()

        # Migration from 5.0:
        service_not_exists = not table_h.column_exist('sale_service') and \
            table_h.column_exist('sale_as_service')

        super().__register__(module_name)

        if service_not_exists:
            query = template.join(product,
                    condition=(product.template == template.id)
                ).select(
                    template.sale_service,
                    where=(
                        (template.sale_as_service) &
                        (product.id == sql_table.product)
                    )
            )
            cursor.execute(*sql_table.update([sql_table.sale_service],
                query, where=(sql_table.sale_as_service)))

    @fields.depends('product', '_parent_product.sale_as_service')
    def on_change_product(self):
        super(SaleLine, self).on_change_product()
        self.sale_as_service = (self.product.sale_as_service
            if self.product else False)
        self.on_change_sale_as_service()

    @fields.depends('sale_as_service', 'product')
    def on_change_sale_as_service(self):
        self.sale_service = None
        if self.sale_as_service and self.product:
            self.sale_service = self.product.sale_service

    @fields.depends('product', '_parent_product.sale_as_service')
    def on_change_with_product_sale_as_service(self, name=None):
        if self.product:
            return self.product.sale_as_service

    def get_invoice_line(self):
        lines = super(SaleLine, self).get_invoice_line()
        for line in lines:
            if not self.sale_as_service:
                continue
            sale_service = self.sale_service
            if sale_service:
                line.product = sale_service
                line.unit = sale_service.default_uom
                # recompute taxes
                line.on_change_product()
        return lines
